# Foundation load_path_management can not be revectored to a development version, 
# becasue it's code is used to do the redirection (to reduce redundancy)
require 'Foundation/load_path_management'
module LoadpathManager
  def self.amend_load_path
    $:.unshift relative('../lib')
    $:.unshift relative('../../magicdraw_plugin_runner/lib/')
    $:.unshift relative('../../uml_metamodel/lib')
    $:.unshift relative('../../umm_driven_builder/lib')
    $:.unshift relative('../../appellation/lib')
    # $:.unshift relative('../../common/lib')
  end
  def self.display_paths
    puts Rainbow("\nProjects running from development directory:").magenta
    found = $:.select do |path|
      project = /(?<=projects\/).+/.match(path)
      puts Rainbow("    #{project.to_s.gsub(/\/lib.*/, '')}").yellow if project
      project
    end
    puts Rainbow("    none").green if found.empty?
    puts
  end
end
