require 'rspec'
require 'rainbow'
require 'pp'
require_relative 'load_path_manager'
LoadpathManager.amend_load_path
LoadpathManager.display_paths
require 'uml_metamodel'
# $generator_test_dir = File.expand_path('../../tmp', __FILE__)
