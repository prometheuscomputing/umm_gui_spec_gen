require 'spec_helper'
require 'fileutils'
require_relative '../lib/spec_gen/spec_builder'
# FileUtils.remove_dir($spec_gen_test_dir) if File.exist?($spec_gen_test_dir)

describe 'spec_gen' do
  # before :all do
  # end
  
  it "should not produce any errors" do
    dsl_file = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
    umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
    message  = SpecBuilder.generate_gui_spec_from_umm_project(umm)
    expect(message).not_to include('Error')
  end

  # This is helpful for spot-checking stuff during development.
  # it "should not produce any errors" do
  #   dsl_file = File.expand_path(relative('../test_data/test.rb'))
  #   umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
  #   message  = SpecBuilder.generate_gui_spec_from_umm_project(umm)
  #   expect(message).not_to include('Error')
  # end
  
  # it "should correctly create associations for primitive properties with a multiplicity > 1" do
  #   dsl_file = File.join(@test_data_dir, 'MultiPrimitivesTest.rb')
  #   umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
  #   message  = SpecBuilder.generate_gui_spec_from_umm_project(umm)
  #   expect(message).not_to include('Error')
  # end
  #
  # it "should correctly create associations for properties typed as enumerations" do
  #   dsl_file = File.join(@test_data_dir, 'EnumTest.rb')
  #   umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
  #   message  = SpecBuilder.generate_gui_spec_from_umm_project(umm)
  #   expect(message).not_to include('Error')
  # end
  #
  # it "should correctly create associations for attributes typed as primitives with multiplicity greater than 1" do
  #   dsl_file = File.join(@test_data_dir, 'MultiPrimitivesTest.rb')
  #   umm      = UmlMetamodel.from_dsl(File.read(dsl_file))
  #   message  = SpecBuilder.generate_gui_spec_from_umm_project(umm)
  #   expect(message).not_to include('Error')
  # end
end