require 'yaml'
require 'umm_driven_builder'
load 'spec_gen/uml_metamodel/to_spec.rb'
class SpecBuilder < UMMDrivenBuilder
  def self.generate_gui_spec_from_umm_project(project)
    new(project).build
  end
  
  def self.mappings
    @mappings ||= YAML.load_file(File.join(File.dirname(__FILE__), 'mappings.yaml'))["mappings"]
  end
  
  def initialize(project)
    super
    @spec              = []
  end
  
  def build
    @project.prepare_spec_gen(self)
    # puts @registry.pretty_inspect"
    order_packages
    order_classifiers
    @ordered_elements.each { |e| @spec << e.to_spec if e.kind_of?(UmlMetamodel::Classifier) }
    @spec.compact!
    write_output
    'No exceptions. Nominal success.' # If you got to this point then at least you didn't raise...
  end
  
  def write_output
    output = covered_packages
    output += homepage_items + "\n\n"
    output += @spec.each(&:rstrip!).join("\n\n") + "\n\n"
    # Save code to files
    model_dir_array = [generated_gem_name, 'lib', generated_gem_name, 'model']
    PrometheusFiles.save_to_file('spec.rb', output, model_dir_array)
  end
  
  def covered_packages
    "covered_packages #{@project.packages(:include_imports => true, :recurse => true).reject { |pkg| pkg.is_a?(UmlMetamodel::Model) }.collect { |pkg| pkg.qualified_name.inspect }.join(', ')}\n\n"
  end
  
  def homepage_items
    roots = @ordered_elements.select { |e| e.get_stereotype('root') }
    organizer = ["\norganizer(:Details, Home) {"]
    homepage_items = []
    roots.each do |r| 
      getter = Appellation.role(r, :multiplicity => :plural)
      # organizer << "  view_ref(:Summary, '#{getter}', :label => '#{Appellation.label(r)}')"
      organizer << "  view_ref(:Summary, '#{getter}', :label => '#{Appellation.label(r, :multiplicity => :plural)}')"
      homepage_items << "homepage_item :'#{r.qualified_name}', :getter => '#{getter}'"
    end
    organizer << "}\n"    
    (homepage_items + organizer).join("\n")
  end
end