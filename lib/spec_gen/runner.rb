load 'spec_gen/spec_builder.rb'
load 'modelGen.rb'

case $Action
when :generate_spec
  messages = "FIXME"
  model = ModelTranslator.generate_ruby_model($SELECTION)
  
  # yaml_file = "/Users/mfaughn/projects/scratch/test_model.yml"
  # File.open(yaml_file, "w") {|f| f.write(model.to_yaml)}
  # STDERR.puts "Done with YAML"
  # data = YAML::load_file(File.open(yaml_file))

  bin_file  = "/Users/mfaughn/projects/scratch/test_model.bin"
  File.open(bin_file, "w") {|f| f.write(Marshal::dump(model))}
  # STDERR.puts "Done with Marshal"
  # data = Marshal::load(File.open(bin_file))
    
  # messages << translator.messages
  # SpecBuilder.generate_spec(data) # FIXME turn me back on!
# when :generate_spec_from_diagram
  # $SELECTION = $Proj.getActiveDiagram.getDiagram
  # SpecBuilder.generate_spec_from_diagram($SELECTION)
else
  puts "INVALID ACTION: #{$Action.inspect}"
end