require 'uml_metamodel'
require 'appellation'
module UmlMetamodel
  class ElementBase
    def prepare_spec_gen(builder)
      # puts "Preparing #{to_dsl}"
      raise "Invalid Element: #{to_dsl}" unless valid?
      builder.register(self)
    end
    def to_spec; end
  end
    
  class Project
    def prepare_spec_gen(builder)
      # arrange_elements! # just makes things nicer, not strictly necessary
      contents.each { |e| e.prepare_spec_gen(builder) }
    end
  end
  
  class Package    
    def prepare_spec_gen(builder)
      super
      contents.each { |e| e.prepare_spec_gen(builder) }
    end
  end
  
  class Classifier
    def prepare_spec_gen(builder)
      super
      contents.each { |e| e.prepare_spec_gen(builder) } # Not sure what this will actually get.  Inner classes?
    end
    
    def to_spec
      attrs, assocs = properties.partition(&:attribute?)
      attr_specs  = attrs.collect  { |a| a.to_spec }.compact
      assoc_specs = assocs.collect { |a| a.to_spec }.compact
      summary = ["summary_view(#{qualified_name}) {"]
      details = ["detail_view(#{qualified_name}) {"]

      providers = parents
      providers += implements if is_a?(UmlMetamodel::Class)
      providers.each do |provider|
        summary << "  attributes_from(#{provider.qualified_name})"
        details << "  content_from(#{provider.qualified_name})"
      end
      
      summary = summary + attr_specs
      attributes_from_summary = "  attributes_from(#{qualified_name})"
      details << attributes_from_summary
      details = details + assoc_specs
      specs = [summary, details]
      assoc_class_properties = properties.select { |prop| prop.association && prop.association.association_class }
      
      assoc_class_properties.each do |acp|
        assoc_class_summary = ["summary_view(#{qualified_name}, :view_name => :#{acp.association.association_class.name}) {"]
        providers.each do |provider|
          assoc_class_summary << "  attributes_from(#{provider.qualified_name})"
        end

        assoc_class_summary << "  attributes_from(#{qualified_name})"

        acp.association.association_class.properties.each do |prop|
          if prop.attribute?
            attr_spec = prop.to_spec(:force_singular => true, :for_assoc_class => true)
            assoc_class_summary << attr_spec
          end
        end
        specs << assoc_class_summary
      end
      
      if association_class?
        association_class_for.properties.each { |prop| details << prop.to_spec(:force_singular => true, :assoc_class_ends => true) }
      end
      specs.each { |s| s << "}" }
      # Just trying to make things pretty.  Can't multiply a string by a negative.
      added_equals = (qualified_name.length > 77) ? '' :  ("=" * (77 - qualified_name.length))
      specs.unshift("# #{qualified_name} " + added_equals)
      specs.flatten.join("\n")
    end    
  end

  class Primitive
    def to_spec; end # don't write out specs for primitive classes
  end
  
  class Property
    def attribute?
      type.primitive? || type.complex_attribute? || type.enumeration?
    end
    
    def to_spec(options = {})
      return if is_derived # we don't add derived attributes? No...
      # We can't automatically add derived attributes to the spec because there is no guarantee that the method is defined.  Generators can't possibly know how to implement a derived accessor.
      return unless is_navigable || attribute? # attributes are always navigable but we're just being extra careful here
      attribute? ? to_attribute_spec(options) : to_association_spec(options)
    end
    
    def to_attribute_spec(options = {})
      prefix = options[:prefix] # not sure why this is here...
      widget_type = get_widget_type
      if widget_type
        role = Appellation.role(self)
        role = "#{prefix}#{role}" if prefix
        spec = "  #{widget_type}('#{role}', :label => '#{Appellation.label(self)}'"
        # This might present a problem if it is a complex_attribute because the client code that we have now might not handle it well
        spec << ", :multiple => true" if upper > 1
        # Needing to put the table_name in here is unfortunate.  Really, the client code should be able to figure this stuff out on its own.
        if options[:for_assoc_class]
          spec << ", :association_class_widget => true, :table_name => :#{Appellation.table_name(owner)}"
        end
        spec << ")"
        spec
      end
    end
    
    def to_association_spec(options = {})
      assoc_class = association.association_class if association && !(options[:assoc_class_ends])
      role  = options[:force_singular] ? Appellation.singular_role(self)  : Appellation.role(self)
      label = options[:force_singular] ? Appellation.singular_label(self) : Appellation.label(self)
      puts self.inspect if label.nil? || label.empty?
      view = assoc_class ? assoc_class.name : 'Summary'
      # FIXME prefer this first way that is currently commented out
      # view_ref = "  view_ref(:#{view}, :getter => :#{role}, :label => '#{label}', :data_classifier => #{type.qualified_name})"
      view_ref = "  view_ref(:#{view}, '#{role}', #{type.qualified_name}, :label => '#{label}'"
      # if it is singular then orderable isn't relevant
      view_ref << ", :orderable => true" if is_ordered && !options[:force_singular]
      view_ref << ")"
      view_ref
    end
    
    # Note: multiple inheritence for primitives is definitely not supported
    def map_primitive_type(type)
      match = SpecBuilder.mappings[type&.name&.downcase]
      return match if match
      if type.parents.any?
        match = map_primitive_type(type.parents.first)
      end
    end
    
    # Returns the literal type for a given UmlMetamodel primitive type
    def get_widget_type
      return 'choice' if type.is_a?(UmlMetamodel::Enumeration)
      if type.primitive?
        widget_type = map_primitive_type(type)
        warn "Unknown type '#{type.name.downcase}' among #{SpecBuilder.mappings.keys}" unless widget_type
      elsif type.complex_attribute?
        widget_type = SpecBuilder.mappings[type.name.downcase]
        warn "Unknown type '#{type.name.downcase}' among #{SpecBuilder.mappings.keys}" unless widget_type
      else
        warn "Unknown type '#{type.name.downcase}' among #{SpecBuilder.mappings.keys}"        
      end
      if widget_type == 'byte'
        warn "'byte' datatype used. It will not be included in the GUI spec.  This may be from BinaryData#data." 
        widget_type = nil
      end
      widget_type
    end
  end
end
